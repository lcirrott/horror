#include "shame.h"

void dangling( int** ptr ) {
  const int N = 100;
  int* var = ( int* ) malloc( N * sizeof( int ) );

  *ptr = var;

  free( var );
}
