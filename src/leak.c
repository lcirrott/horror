#include "shame.h"

void leak() {
  const int N = 1000;
  int* array;
  array = ( int* ) malloc( N * sizeof( int ) );
}

void indirect() {
  const int M = 10;
  const int N = 1000;
  int **darray = ( int** ) malloc( M * sizeof( int* ) );
  for ( int i = 0; i < M; i++ ) {
    darray[i] = ( int* ) malloc( N * sizeof( int ) );
  }
}

void possibly( Grid3D* array, int M ) {
  const int N = 100;
  for ( int i = 0; i < M; i++ ) {
    Grid3D* ptr = &( array[i] );
    ptr->x = ( double* ) malloc( M * sizeof( double ) );
    ptr->y = ( double* ) malloc( M * sizeof( double ) );
    ptr->z = ( double* ) malloc( M * sizeof( double ) );
  }
}
