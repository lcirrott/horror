#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

typedef struct Grid3D {
  double* x;
  double* y;
  double* z;
} Grid3D;

void leak();
void indirect();
void possibly( Grid3D*, int );

void dangling ( int** );

int* stack_use_after_return();
void stack_bounds();

void uninitialized();
int factorial( int );
