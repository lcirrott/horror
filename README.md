[![License: CC BY 4.0](https://img.shields.io/badge/License-CC_BY_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)

All the content of this repository is licensed under the Creative Commons Attribution 4.0 International license (https://creativecommons.org/licenses/by/4.0/).

# Preface

This program contains a gallery of common _bugs_. The purpose is double:
- having a suite of recurrent (sigh) errors and learn to recognize them,
- showing the capability of free/_libre_ debuggers as GDB, Valgrind and AddressSanitizer to find them.

## Download and compilation
- Download and create a build directory:
    ```
    git clone https://gitlab.inria.fr/lcirrott/horror.git
    cd horror
    mkdir build
    guix shell --pure -m manifest.scm
    cd build
    ```
  If you don't have guix, you just need make, CMake and gcc.
- Configure:
  ```
  ccmake ..
  ```
  You will typically need to set the `CMAKE_BUILD_TYPE` variable to `Debug`. Press `[c]` (two times) to configure, `[g]` to generate the configuration files. Additional variables can be set by pressing `[t]`.

- Build:
  ```
  cmake --build . -v
  ```
  The executable will be under  `apps/horror`.

# Some types of "bugs" that are worth considering
Some classical examples:
1. wrong variable values (yes, yes...)
1. [infinite loops](https://en.wikipedia.org/wiki/Infinite_loop)
1. [segmentation faults](https://en.wikipedia.org/wiki/Segmentation_fault)
1. [deadlocks](https://en.wikipedia.org/wiki/Deadlock_(computer_science))

Some more elusive examples:
1. array bounds violation ([stack (or heap, or global) buffer overflow (or underflow)](https://en.wikipedia.org/wiki/Buffer_overflow))
1. [dangling pointers](https://en.wikipedia.org/wiki/Dangling_pointer)
1. stack use after return
1. uninitialized memory (when used in conditional expressions)
1. [memory leaks](https://en.wikipedia.org/wiki/Memory_leak)

> **With a debugger you can**:
> - Avoid recompiling your program each time you want to print a variable (only one recompilation is needed).
> - Interactively control the execution of your program (stop, print, inspect the call stack, restart...).
> - Either print a message or stop the program depending on the value of the program variables.
> - Save your debug session and share it with your colleagues.

> **With a memory-checking tool you can**:
> - Explain weird behaviours of your program (these are often a symptom of invalid memory accesses),
> - Find memory leaks and reduce the memory footprint of your program.

---

# GDB overview
The GNU project debugger is an interactive tool.

How it looks:
    ![gdb](doc/gdb.png)

1. Before starting: compile your program with `-g` (debug symbols). Adding `-O0` (no optimization) typically helps, since backtracing to source file lines becomes easier. Keep in mind that in a typical application you may still want to compile with all the custom optimization options (e.g. `-O3`) either to reproduce the bug or to speed-up the debug.
1. Launching GDB:
`gdb [OPTIONS] [prog|prog procID|prog core]`
It allows to run GDB on a program, or on a process, possibly with a core file.
Useful options:
    - `--args`  to pass command-line arguments to the program,
    - `-x` followed by the path to a file containing some GDB commands to execute.
    - `-ex` followed by a GDB command.
1. The manual is your friend: `man gdb`.
1. First steps:
    - Put a simple breakpoint:
        `break [LOCATION]`
    - Run the program:
        `run`
    - Once the program is stopped, you can move `up` and `down`.
    - Either `continue` the execution (until the next breakpoint, or until the end) or execute the following line (`next`).
    - You can `step` into the function at the current line.
    - Autocompletion is your friend!
    - Abbreviations (often) work.
    - run `info` to see what you can inspect
    - `help [COMMAND]` to discover more.
1. Inspecting your environment
    - `info locals`
    - `frame`
    - `backtrace`
    - `list`
1. Conditional breakpoints and dynamic printfs
    - `break <line_number> if <condition>`
    - `condition <breakpoint_number> <condition>`
    - Using `dprintf` with C-like syntax and breakpoint-like behaviour: `dprintf 24, "My value is %d\n", val`
1. Watchpoints
    - Break when an expression is written into by the program: `watch`
    - Break when an expression is read: `rwatch`
    - Break when an expression is either written into or read by the program: `awatch`
1. Advanced printing:
    - `set print pretty on`
    - "Artificial arrays" https://sourceware.org/gdb/current/onlinedocs/gdb.html/Arrays.html
    - Pretty printers for STL containers https://sourceware.org/gdb/wiki/STLSupport
1. Saving a debug session and reimporting it
    - `save breakpoints [GDBCMDFILE]`
    - `source [GDBCMDFILE]`
1. Redirecting output
    - `set logging file [GDBLOG]`, `set logging on`
    - `run > [PROGLOG]`
1. Activate/deactivate the Text User Interface (TUI) to display the source file
    - Press `Ctrl`-`x` `a`

### Resources
- https://sourceware.org/gdb/documentation/
- https://developers.redhat.com/blog/2021/04/30/the-gdb-developers-gnu-debugger-tutorial-part-1-getting-started-with-the-debugger
- https://developers.redhat.com/articles/2021/10/05/printf-style-debugging-using-gdb-part-1
- https://developers.redhat.com/articles/2021/10/13/printf-style-debugging-using-gdb-part-2
- https://developers.redhat.com/articles/2021/12/09/printf-style-debugging-using-gdb-part-3
- LLDB to GDB commands map: https://lldb.llvm.org/use/map.html
- If you are programming in Python: [pdb](https://docs.python.org/3/library/pdb.html)


## Going parallel
- The old way `mpirun -np [NP] xterm -e gdb --args [MYPROG] [MYARGS]` (https://www.open-mpi.org/faq/?category=debugging#serial-debuggers)
- A new way: https://github.com/Azrael3000/tmpi
- The audacious way: `gdb --pid [PID]`

> On clusters, there are typically also some proprietary parallel interactive debuggers with graphical interfaces. The debugging concepts are the same. If you want to have some fun running an MPI-parallel code inside ARM DDT on PlaFRIM (https://www.plafrim.fr/), allocate some resources with Slurm and try:
> ```
> module load build/cmake/3.15.3 mpi/openmpi/4.0.3
> module load tools/ddt/20.0.3/20.0.3
> git clone https://github.com/MmgTools/ParMmg.git
> cd ParMmg
> cmake -DCMAKE_BUILD_TYPE=Debug -DUSE_VTK=OFF ..
> cmake --build . -j 8
> cd ..
> wget https://gitlab.inria.fr/ParMmg/testparmmg/-/raw/master/A319_gmsh/A319_in_a_box.mesh
> ddt mpirun -np 8 build/bin/parmmg_debug A319_in_a_box.mesh
> ```
> With GDB and xterm, this would be:
> ```
> mpirun -np 8 xterm -e gdb --args build/bin/parmmg_debug A319_in_a_box.mesh
> ```

---

# Valgrind / AddressSanitizer overview
These are tools to analyze the memory usage of your program. These are not interactive, but produce a report with detailed information about memory allocation/deallocation and usage.

## Valgrind
Valgrind is a standalone tool, it only needs that your program is compiled with debug symbols `-g`. A lot of information is produced in the report, the execution time overhead can be huge.

Usage:
```
valgrind --leak-check=full [MYPROG] [MYOPTIONS]
```

It can show:
- Invalid read/write
- "Definitely lost" bytes
- Uninitialized values (`--track-origins=yes`)
- "Still reachable" bytes (`--show-leak-kinds=all`)
- "Possibly"/"indirectly" lost bytes
- Suppressions (`--gen-suppressions=all`, `--suppressions=[filename]`)

How it looks:
![valgrind](doc/valgrind.png)

### Resources
- https://valgrind.org/docs/manual/index.html
- https://developers.redhat.com/blog/2021/04/23/valgrind-memcheck-different-ways-to-lose-your-memory

## Address Sanitizer
AddressSanitizer is integrated in LLVM since version 3.1, in GCC since version 4.8. It needs your program to be recompiled with `-fsanitize=address` (plus debug symbols, and some other options like `fno-omit-frame-pointer`). It can find the same problems as Valgrind (plus more, like stack variables overruns).

```
ASANFLAGS="-fsanitize=address -fno-omit-frame-pointer -O0"
cmake .. -DCMAKE_BUILD_TYPE=Debug -DCMAKE_C_FLAGS="$ASANFLAGS" -DCMAKE_EXE_LINKER_FLAGS="$ASANFLAGS"
cmake --build . -v
```

Usage: just run your recompiled program.

Advanced options: set the environment variable `ASAN_OPTIONS=detect_stack_use_after_return=1` (not always working).

How it looks:
![asan](doc/sanitizer.png)

### Resources
- https://github.com/google/sanitizers/wiki/AddressSanitizer
- https://developers.redhat.com/blog/2021/05/05/memory-error-checking-in-c-and-c-comparing-sanitizers-and-valgrind

---

# Some tips for bug prevention
1. Assertions in C/C++. The `assert()` macro function evaluates a boolean statement. It is only defined if the macro NDEBUG ("no debug") is _not_ defined. Typically, predefined CMake build types pass the option `-DNDEBUG` to the compiler whenever optimized versions are asked.
For example, if we want to check the value of a variable, we can write:
    ```C
    assert( myvar == expectedValue );
    ```
    This line won't be compiled in code optimized with `-DNDEBUG`, but in debug code it will be executed and produce an assertion failure whenever the check fails.

    Assertions can save you a lot of debugging time, especially on out-of-bounds problems. Typical life-saving uses of assertions include:
    - checking that array sizes are compatible when performing vector operations;
    - when filling a vector, checking that the number of filled entries is actually equal to the number of entries;
    - on complicate array index computations, checking that the resulting index is included between 0 and (array size)-1.


2. Always when the NDEBUG macro is not defined, we can introduce in the code some debug sections like:
    ```C
    #ifndef NDEBUG
    /* ... my debug code here ... */
    #endif
    ```
    These sections will _not_ be compiled when NDEBUG is defined. So debug code can be put in there, with the only caveat to be careful not to write into any program variable already in use (otherwise the check itself would change the variable value, and so the flow of the program...). An example can be found here: 
https://github.com/MmgTools/ParMmg/blob/master/src/locate_pmmg.c#L226C0-L266C4 (more complicate code, other than simple counters, could be put in the debug regions of that function, whose conditionals would be otherwise hard to check).

    Whenever possible, a better solution is to write a check function performing only the checks we want and put it into an `assert()`. This function can have program variables as read-only parameters and should return a boolean. In this way, we minimize the possibility of messing up with the normal flow of the program when in debug mode.

3. C++11 has the compile-time assertion `static_assert()` which can be used to check type/class properties during compilation.

4. When a parameter of a function is read-only, use `const` in its declaration and let the compiler check that you are not accidentally modifying a constant parameter. If the parameter is passed through a pointer, declare it as a pointer-to-const.

    The same holds in C++, reference parameters should be changed into reference-to-const whenever it is possible. Similarly, a class method should be made `const` whenever it doesn't modify any class member.

5. Test your code as early and as completely as possible. But this is another topic...


[![License: CC BY 4.0](https://licensebuttons.net/l/by/4.0/80x15.png)](https://creativecommons.org/licenses/by/4.0/)
