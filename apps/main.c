#include "shame.h"

int* array;
Grid3D* gridArray;

int main( int argc, char* argv[] ) {

  /* Compute factorial */
  int var = 5;
  int result = factorial( var );
  printf("The factorial of %d is %d\n",var,result);

  /* Allocate an array */
  const int N = 1000;
  array = ( int* ) calloc( N, sizeof( int ) );

  /* Allocate an array of grids */
  const int M = 4;
  gridArray = ( Grid3D* ) malloc( M * sizeof( Grid3D ) );

  /* Call some dubious functions */
  leak();
  indirect();
  possibly( gridArray, M );
  for ( int i = 0; i < M-1; i++ ) {
    gridArray++;
  }

  int* ptr;
  dangling( &ptr );
  printf("Aaaaahhhhh! %d\n", *ptr);

  int* value = stack_use_after_return();
  printf("Aaaaahhhhh! %d\n", *value);

  stack_bounds();

  uninitialized();

  /* Print random messages */
  printf("Aaaaahhhhh! %d\n", array[N]);
  array[N+1] = 0;

  /* Return */
  return EXIT_SUCCESS;
}
